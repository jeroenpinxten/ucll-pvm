// Declaration of Person
class Person
{
`\NODE{\tt{\bfseries private}:}{private}`
  std::string name;
  int age;

`\NODE{\tt{\bfseries public}:}{public}`
  Person(std::string name, int age);
}`\NODE{;}{semicolon}`
